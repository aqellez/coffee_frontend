import React, {Component} from 'react';

class MachineCoins extends Component{
    constructor(props) {
        super(props);
        this.state = {
           data:[{'a':2},{'b':3}]
        }
     }
componentWillMount() {
    this.props.getMachineCoins();
}
  render() {
      return (
          <div>
              <label ref="machineСoins">{this.props.coins.reduce((prev,next) => parseInt(prev) + parseInt(next.amount),0)}р.</label>
          </div>
      )
  }
}

export default MachineCoins;