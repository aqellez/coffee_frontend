export default function fetchApi(method, url, params){
    if (method === 'GET')
    {
        return(
        fetch(url,{mode:"cors",
        method: 'GET',
        headers: {
          Accept: 'application/json',
        }})
        .then(response => {
            return response.json()})
        )
    }
    if (method === 'PUT')
    { return(
        fetch(url,{mode:"cors",
        method: 'PUT',
        headers: {'Content-Type':'application/json'},
        body : JSON.stringify(params)
        })
    )
    }
}