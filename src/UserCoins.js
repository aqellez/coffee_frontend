import React, {Component} from 'react';


class UserCoins extends Component{
    constructor(props) {
        super(props);
        this.state = {
           coins:[{'a':2},{'b':3}],
           moneySum:0,
        }
     }
    coinClicked(coin_value){
        this.setState({moneySum:this.state.moneySum+coin_value});
        this.props.onUpdate({moneySum:this.state.moneySum});
    }

    oneCoinClicked = () =>{
        this.coinClicked(1);
    } 
    twoCoinsClicked = () =>{
        this.coinClicked(2);
    } 
    threeCoinsClicked = () =>{
        this.coinClicked(5);
    } 
    fourCoinsClicked = () =>{
        this.coinClicked(10);
    }

     
    componentWillMount() {
        this.props.getUserCoins();
        }
    render() {
      return (
          <div>
              <p>Положить монету в автомат: </p>
              <ul>
                <li>
                    <button onClick={this.oneCoinClicked.bind(this)}>1</button>
                </li>
                <li>
                    <button onClick={this.twoCoinsClicked}>2</button>
                </li>
                <li>
                    <button onClick={this.threeCoinsClicked}>5</button>
                </li>
                <li>
                    <button onClick={this.fourCoinsClicked}>10</button>
                </li>
                
            </ul>
            <p>У вас монет:</p>
                <ul>
                    {this.props.coins.map((item, index) =>
                    <li key={index}>{item.coin_type}р: {item.amount}</li>)}                    
                </ul>
                <label>Вы собираетесь положить в автомат: {this.state.moneySum} р.</label>
          </div>
      )
  }
}

export default UserCoins;