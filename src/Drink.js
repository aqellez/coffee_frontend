import React, {Component} from 'react';

class Drink extends Component{
    teaClicked = () =>{
        alert("Вы купили чай!");
    } 
    coffeeClicked = () =>{
        alert("Вы купили кофе!")
    } 
    coffeeWithMilkClicked = () =>{
        alert("Вы купили кофе с молоком!");
    } 
    juiceClicked = () =>{
        alert("Вы купили сок!");
    } 
  render() {
      return (
          <div>
            <li>
                <button onClick={this.teaClicked.bind(this.props)}>Чай</button>
            </li>
            <li>
                <button onClick={this.coffeeClicked}>Кофе</button>
            </li>
            <li>
                <button onClick={this.coffeeWithMilkClicked}>Кофе с молоком</button>
            </li>
            <li>
                <button onClick={this.juiceClicked}>Сок</button>
            </li>
          </div>
      )
  }
}

export default Drink;