import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import PutCoins from './PutCoins.js';
import Drink from './Drink.js';
import UserCoins from './UserCoins.js';
import MachineCoins from './MachineCoins.js';

import fetchApi from './utils.js';

const coins = [{'a':2},{'b':3}];
class App extends Component {
  state = {
          coins:[], 
          machCoins:[], 
          moneySum:0, 
          drinkPrice:0,
          drinkAmount:0, 
        };

  // Обновляем состояние.
  onUpdate(props){
    this.setState({
      moneySum:5
    })
  }

  // Считаем сдачу клиенту.
  exchange(total_money, product)
  {
      var k = total_money - product;
      var change_10 = Math.round(k / 10);
      k -= change_10 * 10;
      var change_5 = Math.round(k / 5);
      k -= change_5 * 5;
      var change_2 = Math.round(k / 2);
      var change_1 = k - change_2 * 2;
      
      return [change_10, change_5, change_2, change_1];

  }

  
  updateCoffeeMachineTotals(params){

  };

  // Обновляем количество напитков в базе.
  updateDrinksAmount(){
    
  }
 

  // Обновляем кол-во монет у пользователя в базе.
  updateUserCoins(params){
    fetchApi('PUT', 'http://localhost:8000/api/user_coins/' + params)

  }

  // Обновляем кол-во монет кофемашины.
  updateMachineCoins(params){
    fetchApi('PUT', 'http://localhost:8000/api/machine_coins/' + params)

  }
  
  // Получаем цену напитка.
  getDrinkPrice(drink_type){
    fetchApi('GET', 'http://localhost:8000/api/drink/?drink_type=' + drink_type)
      .then(drinkPrice => this.setState({ drinkPrice }))
  }

  // Получаем количество напитков.
  getDrinkAmount(drink_amount){
    fetchApi('GET', 'http://localhost:8000/api/drink?drink_amount' + drink_amount)
      .then(drinkAmount => this.setState({ drinkAmount }))
  }

  // Получаем монеты клиента.
  getUserCoins(){
    fetchApi('GET', 'http://localhost:8000/api/user_coins.json','')
      .then(coins =>this.setState({ coins }))
  }

  // Монеты в кофемашине.
  getMachineCoins(){
    fetchApi('GET', 'http://localhost:8000/api/machine_coins.json','')
      .then(machCoins =>this.setState({ machCoins }))
  }

  

  render() {
    return (
      <div>
        <p>Напитки: </p>
          <Drink/>
          <UserCoins getUserCoins = {this.getUserCoins.bind(this)} coins={this.state.coins}
          onUpdate={this.onUpdate.bind(this)}
          />
        <p>Денег в автомате:</p>
          <MachineCoins getMachineCoins = {this.getMachineCoins.bind(this)} 
          coins = {this.state.machCoins}
          coinsSum = {this.state.coinsSum} />
        
      </div>
    );
  }
}

export default App;
